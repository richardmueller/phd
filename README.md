# Content

These are the sources of my Ph.D. thesis “Verifying Responsiveness for Open Systems by Means of Conformance Checking”, defended on 28 August 2014 in Eindhoven, The Netherlands.

# Abstract

Best engineering practices suggest specifying a system before actually implementing it. Both the implementation as well as its specification exhibit behavioral properties. Conformance checking is deciding whether the implementation of a system preserves a certain behavioral property of its specification. This is the central scientific problem of this thesis.

Over the past years, there has been a shift in systems engineering from monolithic, closed systems to distributed systems, composed of open systems. Therefore, our research centers around conformance checking for open systems. An open system interacts with other open systems---that is, its environment. Of particular interest are responsive environments with which interaction or mutual termination is always possible.
We refer to such an environment as a partner. For an open system, conformance checking translates to deciding whether each partner of its specification is a partner of the implementation.

We consider conformance checking for open systems in two distinct scenarios. In the first scenario, the model-model scenario, we assume the specification and the implementation of an open system to be given as formal models. We characterize conformance for two variants of responsiveness. For the first variant, conformance turns out to be undecidable. For the second variant however, we develop a decision algorithm for conformance and a finite characterization of all conforming open systems. In addition, two open systems can be composed, yielding again an (open) system. In general, we require conformance to respect compositionality; that is, we wish to infer the conformance of a composition from the conformance of the composed open systems. Therefore, we also study the above mentioned compositionality property of conformance for the two variants of responsiveness, and show its (un-)decidability.

In the second scenario, the log-model scenario, we assume the specification of an open system to be given as a formal model, but this time no formal model of the implementation is available. However, most implementations record their actual behavior. The observed behavior of an implementation can be recorded in an event log. This is a more realistic and practically relevant assumption because the implementation is often too complex to be formally modeled. The idea is to use an event log to check conformance of the unknown implementation to its known specification. To this end, we present a necessary condition for conformance: We analyze whether there exists a conforming implementation which can produce the event log. Furthermore, we study whether we can discover a formal model of the unknown implementation from the event log, assuming the implementation conforms to its specification.

We implement the decision algorithm from the first scenario and use it to develop algorithms for both questions in the second scenario. We evaluate the implemented algorithms using industrial-sized specifications and event logs.

# Building

`pdflatex rmueller_phd.tex`
`makeindex rmueller_phd.nlo -s nomencl.ist -o rmueller_phd.nls`
`pdflatex rmueller_phd.tex`
`bibtex rmueller_phd`
`pdflatex rmueller_phd.tex`

# Figures

The figures were created using OmniGraffle Pro (http://www.omnigroup.com/products/omnigraffle/) together with Latexit (http://www.chachatelier.fr/latexit/).

# Copyright

* This thesis is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0) License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-sa/4.0/ or send a letter to Creative Commons, 171 Second Street, Suite 300, San Francisco, California, 94105, USA.

* Cover Design and Printing by Uitgeverij BOXPress, Weerdskampweg 11, 5222 BA 's-Hertogenbosch.

* Some adjustments have been made to the classicthesis style which is Copyright (C) 2014 André Miede (http://www.miede.de/index.php?page=classicthesis). He also got his postcard: https://picasaweb.google.com/109927727940437247924/PostcardsForClassicthesis