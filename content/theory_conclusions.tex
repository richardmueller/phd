%!TEX root = ../rmueller_phd.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Conclusions and related work}\label{cha:tout}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\lettrine[findent=.2em,lines=2,nindent=0pt]{I}{n} this chapter, we summarize the results from \autoref{prt:model-model_scenario}.
We compare the compositional conformance and compositional $\bb${\hyph}conformance and classify both into the linear time - branching time spectrum of known preorders between systems in \autoref{sec:tout_classification}.
Finally, we review related work in \autoref{sec:tout_related}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Overview of the results}\label{sec:tout_overview}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We studied a conformance preorder describing whether an open system can safely be replaced by another open system, thereby guaranteeing responsiveness of the overall system.
The latter guarantees the permanent possibility to either mutually communicate or mutually terminate.
In \autoref{cha:resp}, we showed that responsiveness can be seen as a minimal correctness criterion for open systems.
It implies deadlock freedom but does not imply weakly termination.
Besides responsiveness, we also investigated $\bb${\hyph}responsiveness.
The latter requires responsiveness and additionally $\bb${\hyph}boundedness of the composition due to maintaining a previously known message bound $\bb$.
The resulting conformance preorder for each variant of responsiveness is the conformance relation and the $\bb${\hyph}conformance relation, respectively.

Our goal was to analyze the conformance relation and the $\bb${\hyph}conformance relation for compositionality and decidability.
To facilitate this analysis, we characterized both relations using certain denotational semantics for open nets.
\Autoref{fig:tout_model_gen} illustrates the general schema we employed:
First, we provided a denotational semantics for open nets and a refinement relation upon this semantics.
Then, we showed that this refinement relation coincides with the respective conformance relation.
That way, we developed a characterization of the conformance relation.

\begin{figure}[!htb]
  \myfloatalign
  \includegraphics[scale=\thesistopicsize]{figs/introduction_model_gen}
  \caption{The general schema we employed to characterize a conformance relation using denotational semantics for open nets.
  A solid arc illustrates the relation described by the corresponding arc label.
  The dashed arc illustrates logical equivalence.}\label{fig:tout_model_gen}
\end{figure}

\Autoref{tab:tout_chapters_of_model-model} recalls the structure of \autoref{prt:model-model_scenario}.
For each variant of the conformance preorder, we presented a characterization based on a denotational semantics of open nets.
For conformance in \autoref{cha:unbounded}, the semantics, called $\sd${\hyph}semantics, consist of two sets collecting completed traces and unsuccessfully completed traces.
For $\bb${\hyph}conformance in \autoref{cha:bounded}, we had to add the language and a set of uncoverable traces collecting catastrophic traces that cannot be used reliably.
The resulting semantics was the $\bb${\hyph}coverable $\sd${\hyph}semantics.

\begin{table*}[!htb]
    \myfloatalign
    \caption{The structure of \autoref{prt:model-model_scenario} without this chapter.}\label{tab:tout_chapters_of_model-model}
     \begin{tabular}[b]{lccc}
      \toprule
      relation & characterization & compositionality & decidability \\
      \midrule
      conformance & \autoref{cha:unbounded} & \autoref{cha:unbounded} & \autoref{cha:unbounded} \\
      $\bb${\hyph}conformance & \autoref{cha:bounded} & \autoref{cha:compositional} & \autoref{cha:bounded} \& \autoref{cha:compositional} \\
      \bottomrule
      \end{tabular}
\end{table*}

We showed that neither conformance nor $\bb${\hyph}conformance is a precongruence and characterized the coarsest precongruence that is contained in the respective preorder---that is, compositional conformance in \autoref{cha:unbounded} and compositional $\bb${\hyph}conformance in \autoref{cha:compositional}.
In the unbounded setting, we showed in \autoref{cha:unbounded} that conformance and compositional conformance are undecidable.
This motivates our focus on $\bb${\hyph}responsiveness instead of responsiveness.
For the latter, we proved decidability of $\bb${\hyph}conformance in \autoref{cha:bounded} and compositional $\bb${\hyph}conformance in \autoref{cha:compositional}.
In addition, we elaborate a finite characterization of all $\bb${\hyph}conforming open nets for a given open net in \autoref{cha:bounded}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Classifying compositional conformance and compositional $\bb${\hyph}conformance}\label{sec:tout_classification}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we study the relation between compositional conformance and compositional $\bb${\hyph}conformance.
We already showed in \autoref{sub:resp_comparison_conf} that conformance and $\bb${\hyph}conformance are incomparable.
In the following, we use two examples to show that also compositional conformance and compositional $\bb${\hyph}conformance are incomparable.
The first example is used to show that compositional conformance does not imply compositional $\bb${\hyph}conformance.

\begin{example}\label{ex:tout_comparison_conf1}
\Autoref{fig:tout_N4_N5_N6} shows the two open nets $N_4$ and $N_5$ from \autoref{sub:resp_comparison_conf}.
Every fintree failure of the $\fin${\hyph}semantics of $N_4$ is also a fintree failure of the $\fin${\hyph}semantics of $N_5$, because every transition sequence of $\env(N_4)$ is also a transition sequence in $\env(N_5)$, leading to the same markings of $\env(N_4)$ and $\env(N_5)$ except for the place $p_0$.
A token on $p_0$, in turn, does not enable or hinder any further transition.
In other words, we have $\fin(N_4) \subseteq \fin(N_5)$ and, thus, $N_4$ compositionally conforms to $N_5$ by \autoref{def:unbounded_fin-refinement} and \autoref{thm:unbounded_coarsest_precongruence_equals_fin-refinement}.
In contrast, $N_4$ does not compositionally $\bb${\hyph}conform to $N_5$:
We have $\varepsilon \not\in \bd(N_5)$ but $\varepsilon \in \bd(N_4)$ because the place $p_0$ is unbounded in the composition of $N_4$ with any open net.
Thus, $N_4$ does not $\bfin$-refine $N_5$ by \autoref{def:compositional_bfin-refinement}, and \autoref{thm:compositional_coarsest_precongruence_equals_bfin-refinement} shows the statement.
\end{example}

\begin{figure}[htb]
  \ffigbox
  {\begin{subfloatrow}[3]
    \ffigbox[\FBwidth]{\includegraphics[scale=\netsize]{figs/comparison_N4}}{\caption{Open net $N_4$}\label{fig:tout_N4}}
    \ffigbox[\FBwidth]{\includegraphics[scale=\netsize]{figs/comparison_N5}}{\caption{Open net $N_5$}\label{fig:tout_N5}}
    \ffigbox[\FBwidth]{\includegraphics[scale=\netsize]{figs/comparison_N6}}{\caption{Open net $N_6$}\label{fig:tout_N6}}
    \end{subfloatrow}}
  {\caption{Three open nets from \autoref{fig:resp_N4_N5_N6} proving that compositional conformance and compositional $\bb${\hyph}conformance are incomparable.
   In addition to the figures, we have $\Omega_{N_4} = \Omega_{N_5} = \Omega_{N_6} = \{\emset\}$.}\label{fig:tout_N4_N5_N6}}
\end{figure}

With the second example, we show that compositional $\bb${\hyph}conformance does not imply compositional conformance.

\begin{example}\label{ex:tout_comparison_conf2}
Consider the open net $N_6$ in \autoref{fig:tout_N6}.
As in \autoref{ex:resp_comparison_conf2}, we define the open net $N_7$ as the open net $N_6$ in \autoref{fig:tout_N6} but with $\Omega_{N_7} = \{m \in \msets{P_{N_7}} \mid \forall p \in P_{N_7} \setminus \{p_0,p_1\}: m(p) = 0 \}$ as its set of final markings.
The open net $N_6$ compositionally $\bb${\hyph}conforms to $N_7$:
We have $\bd(N_6) = \bd(N_7)$ and $\finbd(N_6) = \finbd(N_7)$ because $N_6$ and $N_7$ differ only in their set of final markings.
Thus, every fintree failure $(w,X,Y)$ of the $\bb${\hyph}bounded $\fin${\hyph}semantics of $N_6$ that is not a fintree failure of the $\bb${\hyph}bounded $\fin${\hyph}semantics of $N_7$ contains a trace $w \in Y$ that reaches a final marking of $N_7$.
By construction, a final marking of $N_7$ is only reachable by a $\bd$-violator of $N_7$.
Therefore, $(w,X,Y) \in \bfin(N_6) \setminus \bfin(N_7)$ implies $(w,X,Y) \in \finbd(N_7)$ and, thus, $\bfin(N_6) = \bfin(N_7)$.
Consequently, $N_6$ $\bfin$-refines $N_7$ and $N_6$ compositionally $\bb${\hyph}conforms to $N_7$ by \autoref{thm:compositional_coarsest_precongruence_equals_bfin-refinement}.

However, $N_6$ does not compositionally conform to $N_7$.
As an example, consider the fintree failure $(a, \emptyset, \{\varepsilon\}) \in \fin(N_6)$.
We have $(a, \emptyset, \{\varepsilon\}) \not\in \fin(N_7)$ because we reach the final marking $[p_0]$ with trace $a$ in $\env(N_7)$.
Therefore, $N_6$ does not $\fin$-refine $N_7$ and does not compositionally conform to $N_7$ by \autoref{thm:unbounded_coarsest_precongruence_equals_fin-refinement}.
\end{example}

With \autoref{ex:tout_comparison_conf1} and \autoref{ex:tout_comparison_conf2}, we showed that compositional conformance and compositional $\bb${\hyph}conformance are incomparable.
In the remainder of this section, we show how compositional conformance and compositional $\bb${\hyph}conformance relate to the known preorders from the linear time - branching time spectrum~\citep{Glabbeek:1990hb,Glabbeek:1993ux}.

\Autoref{fig:tout_classification_known} depicts some of the known preorders from the linear time - branching time spectrum\index{linear time - branching time spectrum} and the relations between them:
bisimulation~\citep{Park:1981du}\index{bisimulation} (B), ready simulation~\citep{Bloom:1995wl}\index{ready simulation} (RS),
must testing~\citep{DeNicola:1984gn}\index{must testing} (MT),
should (or fair) testing~\citep{Natarajan:1995dr,Brinksma:1995di,Rensink:2007ed}\index{should testing}\index{fair testing} (ST),
completed trace\index{completed trace preorder} (CT) and trace\index{trace preorder} (T) preorder.
An arrow (and a sequence of arrows) between two preorders denotes the inclusion relation; for example, the bisimulation preorder implies (is finer than) the ready simulation preorder.
An absent arrow (or sequence of arrows) between two preorders indicates that the inclusion does not hold; for example, the should testing preorder is not finer than the must testing preorder.

\begin{figure}[htb]
  \myfloatalign
  \includegraphics[scale=\spectrumsize]{figs/spectrum_small_known}
  \caption{Some known preorders from the linear time - branching time spectrum.}\label{fig:tout_classification_known}
\end{figure}

\Autoref{fig:tout_classification_all} depicts the classification of compositional conformance and compositional $\bb${\hyph}conformance into the linear time - branching time spectrum from \autoref{fig:tout_classification_known}.
Compositional conformance is the should testing preorder~\citep{Rensink:2007ed} extended with traces that do not lead to a final marking.
For compositional $\bb${\hyph}conformance, we had to extend the should testing preorder by information about bound violations, which make compositional $\bb${\hyph}conformance incomparable to compositional conformance.

\begin{figure}[htb]
  \myfloatalign
  \includegraphics[scale=\spectrumsize]{figs/spectrum_small_all}
  \caption{(Compositional) conformance and (compositional) $\bb${\hyph}conformance classified into the linear time - branching time spectrum.}\label{fig:tout_classification_all}
\end{figure}

If we extend bisimulation in \autoref{def:pre:weak_simulation_bisimulation} to respect final states (i.e., two states $q_1$ and $q_2$ in a bisimulation relation have to satisfy: $q_1$ is a final state if and only if $q_2$ is a final state), then bisimulation implies compositional $\bb${\hyph}conformance.
Compositional $\bb${\hyph}conformance does not imply trace-inclusion (i.e., the trace preorder):
Consider the open nets $N_8$ and $N_9$ in \autoref{fig:tout_N8_N9}.
Every $\bd$-violator of $N_8$ is also a $\bd$-violator of $N_9$, and every trace $w \in L(N_8) \setminus L(N_9)$ is a $\bd$-violator of $N_9$.
In addition, $N_9$ can refuse more traces than $N_8$ because of the missing transition $t_2$.
Therefore, $N_8$ compositionally $\bb${\hyph}conforms to $N_9$, but we have, for example, $ab \in L(N_8)$ and $ab \not\in L(N_9)$.

\begin{figure}[htb]
  \ffigbox
  {\begin{subfloatrow}[2]
    \ffigbox[\FBwidth]{\includegraphics[scale=\netsize]{figs/comparison_N8}}{\caption{Open net $N_8$}\label{fig:tout_N8}}
    \ffigbox[\FBwidth]{\includegraphics[scale=\netsize]{figs/comparison_N9}}{\caption{Open net $N_9$}\label{fig:tout_N9}}
    \end{subfloatrow}}
  {\caption{Two open nets proving that compositional $\bb${\hyph}conformance does not imply trace-inclusion.
   In addition to the figures, we have $\Omega_{N_8} = \Omega_{N_9} = \emptyset$.}\label{fig:tout_N8_N9}}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related work}\label{sec:tout_related}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we review work related to conformance checking, our denotational semantics for open nets, and the undecidability results.

The idea of assigning a formal semantics to a program for verification purposes was introduced by~\citet{Floyd:1967vm} and~\citet{Hoare:1969kg}.
The intuition behind conformance checking derives from program refinement calculi~\citep{Dijkstra:1975vh,Morris:1987vg,Morgan:1988up,Back:1990vi}.
Other names for a conformance relation found in literature are refinement relation~\citep{Wirth:1971tt,Bochmann:1980gw}, implementation relation~\citep{Hoare:1978ww,DeNicola:1984gn,Leduc:1992vg,Tretmans:1996wz}, conformation relation~\citep{Dill:1989vo}, preorder relation~\citep{cleaveland2001equivalence}, accordance relation~\citep{Stahl:2009ej,Aalst:2009gf}, and subcontract relation~\citep{Laneve:2007be,Bravetti:2008vh}, for instance.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Work based on process algebra and declarative models}\label{sub:work_based_on_process_algebra}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\paragraph{Work of Ramajani and Rehof}

\citet{Rajamani:2002ex} define a conformance relation in a bisimulation-like style for the process algebra CCS~\citep{Milner:1989tw}.
Although they use a formal model different than ours, they also investigate asynchronous communication.
\citet{Rajamani:2002ex} investigate stuck-freeness---that is, the behavioral correctness property that guarantees that a message sent by a sender will not get stuck without some receiver ever receiving it, and that a receiver waiting for a message will not get stuck without some sender ever sending it.
In contrast, we consider responsiveness and $\bb${\hyph}responsiveness, which are incomparable to stuck-freeness:
On the on hand, a message may get stuck on a channel without being received for responsiveness, as long as the sender and the receiver continue communicating over other channels.
On the other hand, stuck-freeness does not imply responsiveness because it allows to avoid getting stuck by repeatedly following internal transitions, which does not imply perpetual communication as needed for responsiveness.

\paragraph{Work of Fournet et al.}

\citet{Fournet:2004vv} continue the work of \citet{Rajamani:2002ex} and present with stuck-free conformance a precongruence that excludes deadlocks.
Their precongruence, like compositional conformance and compositional $\bb${\hyph}conformance, is based upon a variation of failures semantics rather than traces.
However, stuckness is more discriminative than deadlock freedom by taking orphan messages into account.
In contrast, responsiveness and $\bb${\hyph}responsiveness allow for orphaned messages if communication continues otherwise (i.e., over other channels).

\paragraph{Work of Padovani et al.}

Padovani et al.~\cite{Laneve:2007be,Castagna:2009jl} introduce the subcontract preorder for CCS-like~\citep{Milner:1989tw} processes without $\tau$-actions.
In contrast, our model for open systems may contain internal transitions (i.e., $\tau$-actions).
Their subcontract preorder is equivalent to must testing~\citep{DeNicola:1984gn} and therefore incomparable to compositional conformance and compositional $\bb${\hyph}conformance (see \autoref{fig:tout_classification_all}).
As an additional difference, their subcontract preorder is an asymmetric notion; that is, it is focusing only on a successful termination of the test (i.e., the system's environment), rather than on the system under test.
In contrast, our notions of responsiveness and $\bb${\hyph}responsiveness are symmetric notions where both composed systems have to terminate successfully.



\paragraph{Work of Bravetti et al.}

\citet{Bravetti:2009gx} extend their previous work in \citep{Bravetti:2007bd,Bravetti:2008vh,Bravetti:2009bi} to asynchronously communicating processes and define the subcontract preorder which preserves weak termination.
Our notions of conformance and $\bb${\hyph}conformance do not preserve weak termination---that is, our preorders are coarser.
The model in~\citep{Bravetti:2009gx} is a modified version of Milner's CCS~\citep{Milner:1989tw} with \emph{one} unbounded but ordered message queue.
In contrast, we use Petri nets with interface places (i.e., open nets) as a model, and \emph{each} interface place models an unbounded unordered message queue.



\paragraph{Work of Dill}

Trace-based semantics like ours (in particular the $\bb${\hyph}bound-ed $\sd${\hyph}semantics and the $\bb${\hyph}coverable $\sd${\hyph}semantics in \autoref{cha:bounded}) where the language is flooded with error traces go back to the work of~\citet{Dill:1989vo}.
Errors in~\citep{Dill:1989vo} arise from communication mismatches and are similar to our $\bd$-violators.
Dill's semantics can be seen as a declarative model and Dill's refinement relations, to which he refers as conformation, are trace inclusions like our characterizations of the conformance and $\bb${\hyph}conformance preorders.
In contrast to our asynchronous (i.e., buffered) setting, Dill considers a synchronous (i.e., an unbuffered) setting.
Similar decision procedures for preorders other than $\bb${\hyph}conformance have also been studied in~\citep{Brinksma:1988tf}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Work based on automata}\label{sub:work_based_on_automata}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\paragraph{Work of de Alfaro and Henzinger}

Interface automata, as defined by \citet{DeAlfaro:2001td}, take up the same ideas as \citet{Dill:1989vo} but on an operational (i.e., automaton) model instead on a declarative one.
In contrast to a refinement relation based on trace inclusions (like the refinement relations of \citet{Dill:1989vo} and the refinement relation we used to characterize conformance and $\bb${\hyph}conformance in \autoref{cha:unbounded} and \autoref{cha:bounded}), refinement of interface automata is characterized by an alternating simulation relation similar to the refinement of modal transition systems~\citep{Larsen:1990ft}.
Alternating simulation is conceptually more complex than refinement based on trace containment.
Further, alternating simulation is overly strong in comparison to our refinements based on the $\sd${\hyph}semantics and the $\bb${\hyph}coverable $\sd${\hyph}semantics, which are the weakest preorders preserving responsiveness and $\bb${\hyph}responsiveness.



\paragraph{Work of Chilton et al.}

\citet{Chilton:2013wp} (a preliminary version appeared as \citep{Chen:2012ie}) formulate a theory for components based on I/O automata~\cite{Lynch:1988tn,Jonsson:1994gt} augmented by an inconsistency predicate on states.
I/O automata are conceptually similar to interface automata by \citet{DeAlfaro:2001td} except that each state is required to be input-enabled.
Like in our setting, system models in~\citep{Chilton:2013wp} can be specified operationally (in our case with open nets, in their case by means of I/O automata), or in a purely declarative manner by means of traces.
They consider with quiescent traces a kind of stop traces and their refinement involves trace containment.
The precongruence defined in~\citep{Chen:2012ie} is based on traces but in a synchronous setting.
In contrast, our precongruences are variants of the should testing preorder~\citep{Rensink:2007ed} in an asynchronous setting.
Moreover, our notion of divergence (i.e., ``infinite internal chatter'') is different from the one in~\citep{Chilton:2013wp}:
If two open systems indefinitely interact with each other, then they are responsive and, hence, we do not treat such a trace as problematic, but \citet{Chen:2012ie} do.
The reason is that, intuitively, we assume a stronger notion of fairness.

Common for the synchronous setting of \citet{Dill:1989vo}, \citet{DeAlfaro:2001td}, and \citet{Chen:2012ie} is that they all have to apply some kind of output pruning (whereas we have not):
In the composition of two open systems, if a sequence of output transitions leads to an error state, these transitions and the states involved have to be removed.
We avoid pruning by introducing the notion of $\bb${\hyph}uncoverable traces in \autoref{cha:bounded}.



\paragraph{Work of Stahl et al.}

Stahl et al.~\citep{Stahl:2009ej,Aalst:2009gf} consider a conformance relation---called accordance---which preserves deadlock freedom on service automata (see \autoref{sec:pre:conclusions}).
The notion of accordance has been first introduced in~\citep{Aalst:2008bu}.
However, the decision procedure for accordance in~\citep{Aalst:2008bu} was limited to acyclic finite state services.
Accordance for deadlock freedom is strictly weaker than our conformance relation for responsiveness, because responsiveness implies deadlock freedom (see \autoref{sec:resp_comparison} for a detailed comparison).
Based on the accordance relation, \citet{Lohmann:2008cw} introduce a single service that encodes a set of services.
This motivates the notion of a maximal $\bb${\hyph}partner in \autoref{sec:bounded_alternative}.
With \autoref{thm:bounded_maximal_b-partner}, we showed how the notion of a maximal $\bb${\hyph}partner can be used to decide $\bb${\hyph}conformance.
The notion of a maximal partner is related to the notion of a canonical dual from~\citep{Castagna:2009jl}.
\citet{Castagna:2009jl} propose a trivial construction method (based on mirroring the message channels) for their restricted setting that does not apply to our setting.



\paragraph{Work of Lohmann et al.}

\citet{Lohmann:2011vx} present a decision procedure for the responsiveness in~\citep{Wolf:2009vd}, but on an automaton model and for a less general variant of responsiveness (see \autoref{sec:resp_conclusions} for more detailed comparison to our variants of responsiveness).
More generally, we deal with open nets that are responsive in some open net compositions but not in others.
Responsiveness in~\citep{Wolf:2009vd,Lohmann:2011vx} is mainly motivated by algorithmic considerations for deciding the respective conformance preorder, but without characterizing the latter semantically or studying compositionality.
Although we are more general, our decision procedure for $\bb${\hyph}conformance in \autoref{cha:bounded} has the same worst case complexity as the decision procedure for responsiveness in~\citep{Lohmann:2011vx}:
\citet{Lohmann:2011vx} propose to compute the operating guideline (i.e., a finite representation of all $\bb${\hyph}partners) of both $\imp$ and $\spec$.
Then, they check for a weak simulation relation of $OG(\imp)$ by $OG(\spec)$ that relates certain state labels of these OG's (i.e., bits representing sets of states in \cite{Lohmann:2011vx}).
In contrast, we propose to compute $\CSD(\imp)$ and $\CSD(\spec)$ and check for a bisimulation between them that respects the state annotations.
Computing $\CSD(N)$ is at most as expensive as computing $OG(N)$ for any open net $N$.



\paragraph{Work of Mooij et al.}

Mooij et al.~\citep{Mooij:2009gz,Mooij:2011is} construct a finite maximal $\bb$-partner---called maximal controller---for the accordance relation of Stahl et al.~\citep{Stahl:2009ej,Aalst:2009gf} (i.e., for deadlock freedom).
In essence, their construction algorithm ``unfolds'' the operating guideline of~\citet{Lohmann:2007gc} into a single service automaton, which results in an exponential blowup of the size of the maximal $\bb$-partner compared to the size of the operating guideline.
Parnjai~\citep{Parnjai:2009vu,Parnjai:2013tl} lifts this construction algorithm to an accordance relation based on a notion of responsiveness that is weaker than ours.
In contrast, our construction of a maximal $\bb$-partner $\max$ from the LTS $\CSD$ in \autoref{sec:bounded_alternative} at most doubles the size of $\max$ compared to the size of $\CSD$.
As the size of $\CSD$ is comparable to the size of an operating guideline (see the previous paragraph), our maximal $\bb${\hyph}partner is in general drastically smaller than the maximal controllers in~\citep{Mooij:2009gz,Mooij:2011is,Parnjai:2009vu,Parnjai:2013tl}.
\citet{Hee:2011dc} show how to compute maximal controllers for weak termination~\citep{Mooij:2010fl,MALIK:2006jd,Bravetti:2008vh}, but only for a subclass of open nets and without providing an implementation.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Work based on Petri nets}\label{sub:work_based_on_petri_nets}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\paragraph{Work of Vogler}
\citet{Vogler:1992wu} presents a few tens of equivalences to support the modular construction of Petri nets.
The setting of his work is more general than ours, as he studies asynchronously communicating (i.e., by fusing places) and synchronously communicating (i.e., by fusing transitions) Petri nets.
As a difference, in the setting of \citet{Vogler:1992wu}, the interface is not separated into input and output places and interface places may be unbounded (like in the $\sd${\hyph}semantics in \autoref{cha:unbounded}).
For open nets with an empty set of final markings, our definitions of responsiveness and conformance yields an equivalence, which is similar to $P$-deadlock equivalence in~\citep{Vogler:1992wu}.
Vogler presents the notion of IR-equivalence for open nets that coincides with should (or fair) testing (called PF++-equivalence in~\citep{Vogler:1992wu}) and, thus, is essentially compositional conformance by \autoref{sec:tout_classification}.
However, IR-equivalence is, as the subcontract preorder of \citet{Laneve:2007be}, an asymmetric notion, whereas our notion of conformance is symmetric.

Our decidability result for compositional $\bb${\hyph}conformance builds upon the decidability of should testing by \citet{Rensink:2007ed}.
We showed how to decide $\fin$-refinement for two finite LTSs, generalizing the construction of \citet[Theorem~61]{Rensink:2007ed} for deciding $\mathcal{F}^+$-refinement.
In the second step, we reduced $\bfin$-refinement under a precondition (i.e., $\bd$-inclusion) to $\fin$-refinement for two finite LTSs.
That way, we can conclude decidability of compositional $\bb${\hyph}conformance as it coincides with $\bfin$-refinement.



\paragraph{Work of Van der Aalst and Basten}

Van der Aalst and Basten~\citep{Basten:2001eh,Aalst:2002co} introduce the notion of projection inheritance for a subclass of Petri nets---that is, workflow nets (WFNs) \citep{vanderAalst:1998tl}.
WFNs are subject to several syntactic restrictions and therefore less general than open nets.
The notion of projection inheritance is based on branching bisimulation and relates two WFNs if they can be substituted.
As a difference, the projection inheritance approach assumes a synchronous communication model (i.e., by fusing transition) and considers soundness as a correctness criterion.
Soundness implies weak termination and, thus, our notions of conformance are strictly coarser than projection inheritance.



\paragraph{Work of Martens}

Martens~\citep{Martens:2004tb,Martens:2005bz} presents a refinement notion for workflow modules---that is, for a Petri net formalism similar to WFNs.
As WFNs, workflow modules are less general than open nets.
To decide refinement of acyclic workflow modules, Martens introduces a data structure called communication graph and a simulation relation on these graphs.
In essence, a communication graph represents the communication behavior of an open system and can be compared with a reduced version of our LTS $\CSD$ without any state labels.
Due to the limitations of workflow modules and the loss of information in communication graphs compared to our LTS $\CSD$, his simulation relation on communication graphs is only sufficient for the accordance notion of Stahl et al.~\citep{Stahl:2009ej,Aalst:2009gf}.



\paragraph{Work of Bonchi et al.}

\citet{Bonchi:2007eb} model the behavior of services using Consume-Produce-Read Nets; another variant of Petri nets.
For their model, they present saturated bisimulation as a refinement relation.
However, saturated bisimulation is too restrictive to allow reordering of sending messages and is, therefore, not suitable for a refinement relation based on asynchronous communication.



\paragraph{Work of Stahl and Vogler}

Our trace-based semantics---the $\sd${\hyph}semantics of an open net---is an adaption of a trace-based semantics with the same name in \citep{Stahl:2011hw,Stahl:2012ca}.
A stop except for inputs we introduced in \autoref{sec:unbounded_preorder} is, in essence, a silent marking~\citep{Mueller_2010_awpn} defined on the composition of two open nets instead on the environment of one open net.
Compared to the work of \citet{Stahl:2012ca} for characterizing conformance with respect to deadlock freedom, finer trace sets are required to characterize the preorders based on responsiveness.
While traces are adequate for the precongruence dealing with deadlock freedom~\citep{Stahl:2012ca}, they do not suffice to characterize the coarsest precongruence for responsiveness, and we had to use some kind of failures instead.
Standard failure semantics was introduced by \citet{Brookes:1984dk}.
By characterizing compositional conformance and compositional $\bb${\hyph}conformance, we use an extension of Vogler's $\mathit{F}^+$-semantics~\citep{Vogler:1992wu} (which \citet{Voorhoeve:2001ha} later introduced as impossible futures).
The corresponding precongruence---that is, compositional conformance---is in essence the should testing preorder~\citep{Natarajan:1995dr,Brinksma:1995di,Rensink:2007ed}.

The construction of the LTS $\BSD$ in \autoref{sec:bounded_decision} is based on a construction with the same name in~\citep{Stahl:2012ca}.
In contrast to \citet{Stahl:2012ca}, our $\BSD$ represents five languages that partition the language $\Sigma^*$ and are not included in each other.
This is because the empty state $\estate$ also arises in the construction of $\BSD$ in~\citep{Stahl:2012ca}, but is not distinguished (in terms of the state labels) from nonempty states.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Work related to the undecidability results}\label{sub:tout_undecidability}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Our undecidability result in \autoref{sec:unbounded_undecidability} is an extract of~\citep{MuellerSV2013_ipl}, where we showed undecidability for the unbounded preorders and precongruences with respect to deadlock freedom, responsiveness, and weak termination.
The results in~\citep{MuellerSV2013_ipl} suggest that the subcontract preorder of \citet{Bravetti:2009gx} is undecidable, although we have no formal proof for this conjecture.

Our proofs in \autoref{sec:unbounded_undecidability} work by reduction from the halting problem of $2$-counter machines using a variation of the ``Jan\v{c}ar-Patterns''~\citep{Jancar:1995wt}.
Counter machines and their halting problem were introduced by \citet{Minsky:1967vk}.
The halting problem for counter machines can be used very naturally to show the undecidability of other problems related to Petri nets, such as bisimilarity and language inclusion~\citep{Jancar:1995wt,Esparza:1998wg}.

The controllability problem for responsiveness~\citep{Wolf:2009vd,Lohmann:2011vx}---that is, the question whether a given open net has at least one partner---is decidable:
There always exists a trivial partner with a loop in which messages are sent without waiting for an answer.
As the corresponding preorder---that is, the conformance relation---is undecidable, we conclude that conformance is a more difficult problem than controllability.
