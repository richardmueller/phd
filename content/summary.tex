%!TEX root = ../rmueller_phd.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Thesis conclusions and outlook}\label{cha:summary}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\lettrine[findent=.2em,lines=2,nindent=0pt]{T}{his} final chapter concludes our thesis.
We summarize the main contributions of our approach for verifying responsiveness for open systems by means of conformance checking in \autoref{sec:summary_contributions}.
In \autoref{sec:summary_limitations}, we discuss open research questions of this approach and theoretical and practical limitations of the presented results.
Finally, \autoref{sec:summary_future_work} sketches ideas for future research.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Summary of contributions}\label{sec:summary_contributions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The central research topic of this thesis was to \emph{verify responsiveness for open systems by means of conformance checking}.
Responsiveness ensures mutual termination or perpetual communication between two systems.
It is a fundamental behavioral correctness criterion for open systems;
a nonterminating composition of two open systems that do not have the possibility to communicate is certainly ill-designed.
In \autoref{cha:resp}, we motivated two variants of responsiveness---responsiveness and $\bb${\hyph}responsiveness---and compared them to other behavioral correctness criteria for open systems.
The notion of $\bb${\hyph}responsiveness is a variant of responsiveness where the number of pending messages never exceeds a previously known bound $\bb$.
Although respecting a bound $\bb$ may seem restricting, $\bb${\hyph}responsiveness is practically relevant:
Distributed systems operate on a middleware with buffers that are of bounded size.
The actual buffer size can be the result of a static analysis of the underlying middleware or of the communication behavior of an open system, or simply be chosen sufficiently large.

A conformance relation for responsiveness describes when one open system (i.e., the specification) can be safely replaced by another open system (i.e., the implementation) without affecting responsiveness with an unknown environment (i.e., other open systems called partners).
We defined the conformance relations for responsiveness and $\bb${\hyph}responsiveness---that is, conformance and $\bb${\hyph}conformance---and the coarsest precongruences contained therein---that is, compositional conformance and compositional $\bb${\hyph}conformance.

We aimed to verify responsiveness by means of conformance checking in two distinct scenarios---the model-model scenario and the log-model scenario---each of which we investigated in a separate part of this thesis.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The model-model scenario}\label{sub:summary_the_model_model_scenario}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For the model-model scenario, we assume that both the specification and the implementation of an open system are given as formal models.
Then, we can verify responsiveness by checking for conformance between the two formal models.

In \autoref{cha:unbounded}, we analyzed conformance and compositional conformance in detail.
We provided open nets with the $\sd${\hyph}semantics and showed that set-wise inclusion of the $\sd${\hyph}semantics characterizes conformance.
In addition, we detailed that compositional conformance cannot be characterized with the $\sd${\hyph}semantics or, in general, a denotational semantics weaker than standard failures semantics.
Therefore, we provided open nets with the $\fin${\hyph}semantics (i.e., an extension of standard failure semantics) and showed that refinement on the $\fin${\hyph}semantics characterizes compositional conformance.
Based on the characterizations of conformance and compositional conformance, we showed that both relations are undecidable.

In \autoref{cha:bounded}, we investigated the $\bb${\hyph}conformance relation.
We provided open nets with a trace-based semantics---the $\bb${\hyph}coverable $\sd${\hyph}semantics---and showed that set-wise inclusion of the $\bb${\hyph}coverable $\sd${\hyph}semantics characterizes $\bb${\hyph}conformance.
Giving an answer to an open question, we showed that $\bb${\hyph}conformance is strictly larger than compositional $\bb${\hyph}conformance (i.e., $\bb${\hyph}conformance is a preorder but not a precongruence).

In contrast to conformance, $\bb${\hyph}conformance is decidable.
Thus, we elaborated a decision procedure to decide whether an open net $\imp$ $\bb${\hyph}conforms to an open net $\spec$ based on two LTSs $\CSD(\imp)$ and $\CSD(\spec)$.
For a given open net, we additionally developed a finite characterization of all $\bb${\hyph}conforming open nets based on the notion of a maximal $\bb${\hyph}partner; this finite characterization serves as an alternative decision procedure for $\bb${\hyph}conformance.

In \autoref{cha:compositional}, we investigated compositional $\bb${\hyph}conformance---that is, the coarsest precongruence that is contained in the $\bb${\hyph}conformance relation.
We provided open nets with a failure-based semantics (the $\bb${\hyph}bounded $\fin${\hyph}semantics) and showed that refinement on the $\bb${\hyph}bounded $\fin${\hyph}semantics characterizes compositional $\bb${\hyph}conformance.
Based on our characterization, we proved compositional $\bb${\hyph}conformance to be decidable by reducing it to deciding should testing.
Thereby, the decision procedure presented in \autoref{cha:compositional} does not depend on open nets but is independent from the concrete model.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The log-model scenario}\label{sub:summary_the_log_model_scenario}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For the log-model scenario, we assume the specification of an open system to be given as a formal model, but no formal model of the implementation is available.
Instead, we assume that observed behavior of the running but unavailable implementation is given in the form of an event log.

In \autoref{cha:testing}, we presented a testing approach for $\bb${\hyph}conformance.
Testing for $\bb${\hyph}conformance can show that the implementation does not $\bb${\hyph}conform to the specification if the event log contains some erroneous behavior.
To this end, we elaborated a necessary condition for $\bb${\hyph}conformance of the implementation $\imp$ to the specification $\spec$ based on the open net $\mp(\max(\spec))$ of the specification $\spec$:
If the event log cannot be replayed on the environment of $\mp(\max(\spec))$, then $\imp$ does not $\bb${\hyph}conform to $\spec$.
We showed the existence of the open net $\mp(\max(\spec))$ and demonstrated that it can be automatically constructed.

In \autoref{cha:discovery}, we presented a technique to discover a system model of an unknown implementation from a given system model $\spec$ and observed behavior of that implementation interacting with its environment.
Our technique produces an open net $\imp$ that $\bb${\hyph}conforms to $\spec$ and, in addition, balances four conflicting quality dimensions:
fitness, simplicity, precision, and generalization.
As an additional improvement, we proposed an abstraction technique to reduce the infinite search space to a finite one.
We can also apply our approach to discover a $\bb${\hyph}partner $C$ of an open net $N$ such that $C$ has, among the set of all $\bb$-partners of $N$, high quality with respect to $N$ and a given event log.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Tool support}\label{sub:summary_tools}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We proposed several algorithms in this thesis:
For the model-model scenario, we defined verification algorithms to decide $\bb${\hyph}conformance.
For the log-model scenario, we defined a testing algorithm and a discovery algorithm.
All algorithms of this thesis have been prototypically implemented in software tools.
These tools follow a ``one tool - one purpose'' policy, which has been proven helpful in implementing a theory of correctness for open systems~\citep{Lohmann:2010ws}.
In this thesis, we used the following tools:

\begin{description}
  \item[Chloe] is a tool to represent the semantics of an open net $N$.
  It represents the $\bb${\hyph}bounded $\sd${\hyph}semantics and the $\bb${\hyph}coverable $\sd${\hyph}semantics of $N$ by computing the LTSs $\BSD(N)$ and $\CSD(N)$, respectively (see \autoref{cha:bounded}).
  As a side-effect of computing $\CSD(N)$, Chloe can output the LTS $\MP(N)$ with or without Boolean annotation, the most-permissive $\bb${\hyph}partner $\mp(N)$, and the maximal $\bb${\hyph}partner $\max(N)$ of $N$;
  these three artifacts serve as a basis for conformance testing (see \autoref{cha:testing}) and system discovery (see \autoref{cha:discovery}).
  We use version 2.0~\citep{url_chloe}, which is licensed under the GNU Affero General Public License.

  \item[Delain] is a tool to decide whether an open net $\imp$ $\bb${\hyph}conforms to an open net $\spec$.
  To this end, Delain checks for set-wise language inclusion of their respective $\bb${\hyph}coverable $\sd${\hyph}semantics using the previously computed LTS $\CSD(\imp)$ and $\CSD(\spec)$ (see \autoref{cha:bounded}).
  We use version 0.3~\citep{url_delain}, which is licensed under the GNU Affero General Public License.

  \item[Locretia] is a tool to randomly create an artificial event log $\elog$ from an open net $N$, using either the viewpoint of $N$ or the viewpoint of $N$'s environment (see \autoref{cha:testing}).
  We used artificial event logs for evaluating our approaches for conformance testing (see \autoref{cha:testing}) and system discovery (see \autoref{cha:discovery}).
  As a side-effect, Locretia can output the labeled nets $\env(N)$ and $\inner(N)$ for any open net $N$.
  We use version 1.1~\citep{url_locretia}, which is licensed under the GNU Affero General Public License.

  \item[Service Discovery] is a tool for open system discovery:
  Given an open net $\spec$ and an event log $\elog$ with observed behavior of an unknown implementation of $\spec$,
  we can discover an open net $\imp$ that $\bb${\hyph}conforms to $\spec$ and, in addition, has high or even highest quality with respect to $\elog$ and $\spec$ (see \autoref{cha:discovery}).
  Service Discovery~\citep{url_servicediscovery} is a ProM plug-in licensed under the GNU Public License.
  
  \item[CSVExport] is an event listener for Apache ODE~\citep{url_ode}.
  CSVExport outputs sent or received messages from the viewpoint of a deployed WS-BPEL process together with the identifier of the corresponding process instance and a timestamp into comma-separated values.
  Comma-separated values in turn can be imported as event logs into ProM~\citep{url_prom}.
  CSVExport~\citep{url_csvexport} is licensed under the Apache License version 2.

  \item[Eclipse BPEL Designer] adds comprehensive support for the definition, authoring, editing, deploying, testing and debugging of WS-BPEL processes to the well-known integrated development environment (IDE) Eclipse~\citep{url_eclipse}.
  We use version 1.0.3~\citep{url_bpel_designer}, which is licensed under the Eclipse Public License.

  \item[BPEL2OWFN] is a tool to translate WS-BPEL processes to open nets~\citep{Lohmann:2008uk}.
  We use version 2.4, which is licensed under the GNU Affero General Public License.

  \item[Apache ODE] executes WS-BPEL processes by communicating with other (web) services, manipulating data, and handling exceptions.
  We use Apache ODE version 1.3.6~\citep{url_ode} on an Apache Tomcat server version 8.0.3~\citep{url_tomcat};
  both are licensed under the Apache License version 2.

  \item[SoapUI] is a web service testing application for service-oriented architectures.
  Its functionality covers web service inspection, invoking, development, simulation, mocking, functional testing, and load and compliance testing.
  We use version 4.6.4~\citep{url_soapui}, which is licensed under the GNU Lesser General Public License.

  \item[ProM] is an extensible plugin-based framework that supports a wide variety of process mining techniques.
  Replaying an event log on a labeled net can be done with the plug-in ``PNetReplayer'' that implements the $A^*${\hyph}algorithm~\citep{Adriansyah:2011ta}.
  We use version 6.3~\citep{url_prom}, which is licensed under the GNU Public License.
\end{description}

The first five tools (Chloe, Delain, Locretia, Service Discovery, and CSVExport) were originally developed to conduct the experiments presented in this thesis.
These experiments proved the basic applicability of the results of this thesis (see \autoref{sec:bounded_implementation}, \autoref{sec:testing_evaluation}, \autoref{sec:discovery_results}, and \autoref{cha:case}) using open nets and event logs of industrial size on a computer with average computing power.
\Autoref{tab:summary_tools} relates the used tools and their purpose.
Existing tools are mainly used to derive the inputs to the proposed algorithms (e.g., open nets and event logs),
whereas the originally developed tools primarily implement the algorithms.

\begin{table*}[!htb]
  \myfloatalign
  \caption{The tools used in the thesis.}\label{tab:summary_tools}
  \begin{tabular}[b]{lrr}
  \toprule
  purpose & original tools & reused tools \\
  \midrule
  \multirow{2}{*}{derive open nets} & & BPEL Designer, \\
    & & BPEL2OWFN \\
  \cmidrule(lr){2-3}
  \multirow{2}{*}{derive event logs} & CSVExport & Apache ODE, \\
    & & SoapUI \\
  \cmidrule(lr){2-3}
  \multirow{2}{*}{decide $\bb${\hyph}conformance (\autoref{cha:bounded})} & Chloe, & \\
   & Delain & \\
  \cmidrule(lr){2-3}
  \multirow{2}{*}{test for $\bb${\hyph}conformance (\autoref{cha:testing})} & Chloe, & ProM \\
    & Locretia & \\
  \cmidrule(lr){2-3}
  \multirow{2}{*}{high-quality discovery (\autoref{cha:discovery})} & Chloe, & ProM \\
   & Service Discovery & \\
  \bottomrule
  \end{tabular}
\end{table*}

All tools used in this thesis---either developed under the course of this thesis, or existing tools---are free and open source software.
This greatly eases the usage of the presented approaches.
An integration of the developed tools into industrial modeling tools and acceptance tests are out of scope of this thesis.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Limitations and open questions}\label{sec:summary_limitations}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we discuss open research questions of our approach and the theoretical and practical limitations of the presented results.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Incomplete or unsound specifications}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this thesis, we focused on verifying responsiveness for open systems by means of conformance checking in two different scenarios:
In the model-model scenario, we assumed that both the specification and the implementation of an open system are given as formal models, and
in the log-model scenario, we assumed that the specification is given as a formal model and we have observed behavior of the unknown implementation in form of an event log.
Thus, in both scenarios, we are given a formal specification.
The inherent assumption of conformance checking is that this formal specification is \emph{valid} with respect to the system we want to implement:
We assume that the specification represents exactly the intended system design, and
we then verify that the implementation is an intended system design by checking whether the implementation conforms to the specification.

However, the assumption of a valid specification may not hold for complex specifications.
Like other engineering processes, constructing a formal specification is a difficult and error-prone task~\citep{Konighofer:2013kk}.
Errors in the formal specification $\spec$ can lead to two flaws:
(1) A specification may be \emph{incomplete}; that is, it allows for implementations $\imp$ that do not represent the intended system design.
(2) A specification may be \emph{unsound}; that is, it disallows implementations $\imp$ that actually do represent intended system design.
As conformance checking inherently relies on the validity of the specification, we cannot decide whether $\imp$ is an actual intended design or not by verifying that $\imp$ conforms to $\spec$.
However, this is not a specific limitation of conformance checking, but a general limitation of formal methods~\citep{Hall:1990dv,Tretmans:2001uy}.
Somewhere in system development, the link to the informal reality (i.e., the intended system) has to be made, and the validity of this link (that is, the formal specification represents the intended system design) can only be assumed, not proved.
The challenges of incomplete and unsound specifications have been already addressed before in various ways and there exists a rich body of 
literature, e.g.,~\citep{Katz:1999kd,Yoshiura:2004eo,Pill:2006gg,Claessen:2007im,Grosse:2007kb,Cimatti:2008bh,Konighofer:2013kk}.

A recent approach to circumvent this problem is the notion of quality of an implementation to its specification~\citep{Almagor:2013ti}:
Here, the traditional Boolean verification problem (e.g., $\imp$ either conforms to $\spec$, or not) is substituted by a multi-valued problem by introducing a quantitative aspect to verification (e.g., to which extend $\imp$ conforms to $\spec$).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Measuring quality is subjective}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Given a specification $\spec$ and an event log $\elog$, our discovery approach in \autoref{cha:discovery} computes an open net $\imp$ that $\bb${\hyph}conforms to $\spec$ and has high quality with respect to $\elog$ and $\spec$.
The quality of $\imp$ with respect to $\elog$ and $\spec$ is the weighted average over the four quality dimensions fitness, simplicity, precision, and generalization.
For measuring simplicity, we compare the size of $\inner(\imp)$ with the size of the smallest subsystem $G$ of $\MP(\max(\spec))$ that weakly simulates $\graph(\inner(\imp))$.
However, this simplicity metric is not precise enough:
There exist cases in which $\inner(\imp)$ is even smaller than $G$ due to ``unrolled loops'' in $\MP(\max(\spec))$ (see also \autoref{sec:discovery_improvement}).
Then, $\imp$ is not distinguished in terms of simplicity from an open net $\imp'$ that $\bb${\hyph}conforms to $\spec$ and whose inner net's size is equal to $G$; both $\imp$ and $\imp'$ have simplicity $1$.

One idea to address this limitation is to change the simplicity metric such that the size of $\inner(\imp)$ is compared with the smallest subsystem $G$ of $\MP(\max(\spec))$ that weakly simulates $\graph(\inner(\imp))$ and is reduced modulo $\bb${\hyph}conformance.
That way, we could ensure that $\inner(\imp)$ (and, therefore, $\imp$) cannot be smaller than the respective subsystem of $\MP(\max(\spec))$.
However, it is an open question how to do this.
Another idea is to consider concurrency in the simplicity metric.
To this end, we have to transform the LTS $G$ into a Petri net and somehow compare it to $\imp$.
However, transforming $G$ into a Petri net may come at the price of drastically increasing runtimes, even when applying state-of-the-art tools~\citep{Carmona:jo}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Abstraction only preserves fitness and simplicity}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We showed \autoref{cha:discovery} that, in general, our genetic discovery algorithm produces better results (i.e., a higher quality of the discovered open net in less time) on the finite abstraction of the search space (i.e., using $\bb${\hyph}subnets of $\spec$) than on the complete search space (i.e., using arbitrary open nets that $\bb${\hyph}conform to $\spec$).
However, our proposed abstraction technique---the $\bb${\hyph}subnets of $\spec$---only preserves fitness and simplicity;
the values of the precision and the generalization dimensions may be higher for arbitrary open nets that $\bb${\hyph}conform to $\spec$.
It is an open question how the abstraction technique based on $\bb${\hyph}subnets can be improved such that it preserves all four quality dimensions, and how a more precise abstraction technique would influence the quality of the discovered open nets.

An idea to circumvent the problem of excluding open nets with high generalization and precision from the search space (by restricting the search space to $\bb${\hyph}subnets) is to post-process the discovered $\bb${\hyph}subnet $\imp$ of $\spec$.
By \autoref{def:discovery:b-subnet}, $\imp$ may not have the highest precision due to a ``furled'' loop in its inner net's reachability graph.
The idea is to subsequently (i.e., after discovering $\imp$) unroll that loop to increase precision, thereby transforming the $\bb${\hyph}subnet $\imp$ to an open net $\imp'$ that is no longer a $\bb${\hyph}subnet of $\spec$ but still $\bb${\hyph}conforms to $\spec$.
This post-processing may increase the precision of $\imp'$ while preserving its fitness, simplicity, and generalization;
in other words, the quality of $\imp'$ may be higher than the quality of $\imp$ with respect to $\spec$ and the given event log.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Future work}\label{sec:summary_future_work}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we sketch directions for possible extensions of the work presented.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Refined conformance relations}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this thesis, we motivated and fixed responsiveness as a fundamental behavioral correctness criterion for interacting open systems.
Once correctness with respect to responsiveness is established for an implementation, one can easily think of additional criteria that should hold:
An example is Microsoft's asynchronous event driven programming language P~\citep{Desai:2013ex},
which---in addition to responsiveness---requires that \emph{no message in any message channel is ignored forever}.
However, this additional criterion induces a conformance relation that is different from the ones considered in this thesis.
Another issue is the minimal requirement \emph{weak termination} (e.g.,~\citep{Mooij:2010fl,MALIK:2006jd,Bravetti:2008vh}):
Reaching a final state should always be possible.
This criterion is very close to the idea of should testing, but it is not clear how to characterize the respective conformance relation (which is a precongruence itself).
In contrast, we characterized precongruences related to responsiveness---that is, compositional conformance and compositional $\bb${\hyph}conformance---with semantical ideas that also worked for should testing.
Another idea is to extend responsiveness to also consider communication over ports (e.g., as for web service interfaces defined in WSDL~\cite{Christensen:2001wh}) in the sense that, for every port $P$, it should always be possible to communicate over $P$.

In a more general view, we imagine arbitrary correctness criteria that are described as temporal formulae, e.g., in CTL*~\citep{Pnueli:1977fg}, and which should hold in the composition of two open systems.
It is an interesting research question whether the approaches for conformance checking in this thesis can be generalized to deal with behavioral correctness criteria formulated as temporal formulae.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Improved algorithms}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In \autoref{cha:bounded}, we showed how to decide whether an open net $\imp$ $\bb${\hyph}conforms to an open net $\spec$ based on the LTSs $\CSD(\imp)$ and $\CSD(\spec)$, or with help of the maximal $\bb${\hyph}partner $\max(\spec)$ of $\spec$.
Although we showed that computing $\CSD$ is feasible for open nets of industrial size (see \autoref{sec:bounded_implementation}), the construction algorithm, in general, suffers from the state-space explosion problem~\citep{Valmari:1998eh}.
There exist several effective state-space reduction techniques for verification~\citep{Valmari:1998eh}.
It is an interesting research question how these techniques can be employed to speed up the construction of $\CSD$ or $\max(\spec)$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Compositionality in the log-model scenario}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In \autoref{cha:testing}, we presented a passive testing approach for $\bb${\hyph}conformance:
Given a specification $\spec$ and an event log $\elog$ that derives from an implementation $\imp$,
we can construct an open net $\mp(\max(\spec))$ and showed that if $\elog$ cannot be replayed on $\mp(\max(\spec))$, then $\imp$ does not $\bb${\hyph}conform to $\spec$.
In \autoref{cha:discovery}, we presented a system discovery approach for $\bb${\hyph}conformance:
Given a specification $\spec$ and an event log $\elog$, who showed how to compute an open net $\imp$ that $\bb${\hyph}conforms to $\spec$ and has high or even highest quality with respect to $\elog$ and $\spec$.
Thereby, the presented implementation depends on several inputs, among others the LTS $\MP(\max(\spec))$ that we can compute from $\spec$.
In other words, both approaches in \autoref{cha:testing} and \autoref{cha:discovery} rely on the maximal $\bb${\hyph}partner $\max(\spec)$ of $\spec$; this is because $\max(\spec)$ finitely characterizes all $\bb${\hyph}conforming open nets of $\spec$ (see \autoref{cha:bounded}).
Therefore, both approaches in \autoref{cha:testing} and \autoref{cha:discovery} are currently limited to $\bb${\hyph}conformance and cannot be applied to compositional $\bb${\hyph}conformance:
It is an open and interesting research question whether there exists a ``maximal'' $\bb${\hyph}partner of $\spec$ that finitely characterizes all open nets $\imp$ that compositionally $\bb${\hyph}conform to $\spec$.
Such a $\bb${\hyph}partner may also serve as an alternative to decide compositional $\bb${\hyph}conformance, just like the maximal $\bb${\hyph}partner $\max$ serves as an alternative decision procedure for $\bb${\hyph}conformance (see \autoref{sec:bounded_alternative}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Refined discovery}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In \autoref{cha:discovery}, we steer the genetic discovery algorithm by user-given weights to the four quality dimensions fitness, simplicity, precision, and generalization.
These four quality dimensions compete with each other and their interplay is of a complex nature, as shown in~\citep{Buijs:2012jr}.
Consequently, it is an interesting research question to study the impact of different weights of the quality dimensions on the quality of the discovered $\bb${\hyph}conforming open net.

Another problem is that there are certain disadvantages to measuring the quality of the discovered $\bb${\hyph}conforming open net $\imp$ by assigning user weights to the quality dimensions and aggregating them into a single quality measure:
For example, determining the weights upfront is difficult if structural changes on $\imp$ have unknown or too complex effects on the value of a single quality dimension.
Another disadvantage is that by returning only $\imp$, the user is not provided with any insights in the trade-offs between the quality dimensions.
One idea to overcome these problems is to return a Pareto front of $n$ discovered $\bb${\hyph}conforming open nets $\{\imp_1, \ldots, \imp_n\}$ instead of a single open net $\imp$:
A Pareto front is a set of \emph{mutually non-dominating} open nets, whereas an open net $\imp_i$ dominates an open net $\imp_j$ (for $i, j \in \N$) if, for all quality dimensions, the quality of $\imp_i$ is equal to or higher than the quality of $\imp_j$, and for one quality dimension, the quality of $\imp_i$ is strictly higher than the quality of $\imp_j$~\citep{VanVeldhuizen:1998ub}.
Recently, this idea was successfully employed to a discovery algorithm in the area of process mining~~\citep{Buijs:up}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Introducing additional aspects}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this thesis, we entirely focused on the communication protocol of an open system and abstracted from other aspects such as the location of the open system, the underlying middleware, instantiation of the open system and the correlation of messages, the content of messages, or nonfunctional properties (e.g., time).
Especially the abstract concept of time is crucial for many real-world systems;
hence, there exist numerous approaches to incorporate time for example in workflow systems~\citep{Bettini:2002tg}, web services~\citep{DuskeMueller_2012_zeus}, or any kind of protocol~\citep{Ponge:2010eh}.
These aspects are not considered during conformance checking, and their integration into our conformance checking approach would broaden the applicability of our results.
