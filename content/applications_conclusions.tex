%!TEX root = ../rmueller_phd.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Conclusions and related work}\label{cha:aout}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\lettrine[findent=.2em,lines=2,nindent=0pt]{I}{n} this chapter, we summarize the results from \autoref{prt:log-model_scenario}.
In addition, we review work that is related to conformance testing in \autoref{sec:aout_conformance_testing} and
work that is related to open system discovery in \autoref{sec:aout_system_discovery}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Overview of the results}\label{sec:aout_results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We studied a conformance relation between two open systems in the log-model scenario.
In the log-model scenario, the formal model of only one open system---the specification---is given, but no formal model of the second open system---the implementation---is available.
Instead of a formal model of the implementation, we use the observed behavior of the implementation as input.
We referred to the latter as the event log.
\Autoref{fig:aout_log-model} illustrates again our assumptions for the log-model scenario.

\begin{figure}[!htb]
  \myfloatalign
  \includegraphics[scale=\thesistopicsize]{figs/introduction_log2}
  \caption{The log-model scenario}\label{fig:aout_log-model}
\end{figure}

In the log-model scenario, we followed two goals.
Our first goal was to investigate how to use the event log to check $\bb$-conformance of the unknown implementation to the given specification.
To this end, we proposed a testing approach based on a necessary condition for $\bb${\hyph}conformance in \autoref{cha:testing}.
We analyzed whether there exists a $\bb${\hyph}conforming implementation which may produce the behavior seen in the event log without any mismatches.
Thereby, we used the finite characterization of all $\bb${\hyph}conforming open nets, in the form of a maximal $\bb${\hyph}partner, that we developed in \autoref{prt:model-model_scenario}.
We demonstrated our testing approach using industrial-sized specifications and event logs, and the tools from \autoref{prt:model-model_scenario}.

Our second goal was to support the design of $\bb${\hyph}responsive open systems in the log-model scenario by discovering a formal model of the unknown implementation based on the given event log.
To this end, we presented a discovery technique in \autoref{cha:discovery}.
We produced a system model for $\imp$ that $\bb${\hyph}conforms to $\spec$ and, in addition, balances the four conflicting quality dimensions
fitness, simplicity, precision, and generalization.
As an additional improvement, we proposed an abstraction technique to reduce the infinite search space to a finite one, and evaluated the discovery algorithm with and without the abstraction technique using industrial-sized specifications and event logs.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Work related to conformance testing}\label{sec:aout_conformance_testing}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we review work related to our conformance testing approach in \autoref{cha:testing}.

Our conformance testing approach assumes recorded behavior (i.e., an event log) of the implementation to be given, and employs this recorded behavior to test for conformance of the unknown implementation to the known specification.
Here, techniques are adapted from process mining~\citep{vanderAalst:2011wg}.
Process mining techniques focus on extracting process models from event logs (``process discovery''),
comparing normative models with the reality recorded in event logs (which is also called ``conformance testing''~\citep{Rozinat:2006el} or ``conformance checking''~\citep{Aalst:2005cj,Aalst:2008jc,Rozinat:2008iz,Adriansyah:2011ta,vanderAalst:2012kf}), and extending models based on event logs (``extension'').
In the following, we give a brief overview of conformance checking in process mining.

The goal of conformance checking in process mining is to find commonalities and discrepancies between the modeled behavior and the observed behavior of a process~\citep{vanderAalst:2011wg}.
\citet{Cook:1999je} compare event traces with process models to measure their similarity.
The similarity of an event trace $w$ and a process model $N$ is quantified by the number of insertions and deletions that are necessary to transform $w$ into a trace of $N$;
this is, in essence, the idea behind the alignments~\citep{Adriansyah:2011ta,vanderAalst:2012kf} that we used in \autoref{cha:testing} and \autoref{cha:discovery}.
Later, \citet{Cook:2001cr} extended their approach to also consider time aspects.
Rozinat et al.~\citep{Rozinat:2006el,Rozinat:2008iz} propose a token-based replay approach to measure the fitness of an event trace $w$ and a labeled net $N$:
$w$ is replayed on $N$ by adding necessary tokens (i.e., missing tokens in the preset of a transition) and removing superfluous tokens (i.e., remaining tokens in the postset of a transition).
A fitness metric is then calculated based on the number of added, superfluous, produced, and consumed tokens.
In contrast to alignments, the fitness metric in \citep{Rozinat:2008iz} is sensitive to the structure of $N$.
\citet{Goedertier:2009wl} augment an event trace $w$ with artificial negative events before comparing $w$ to a labeled net $N$ in a way similar to~\citep{Rozinat:2008iz};
negative events are then used to quantify the precision of $w$ and $N$.
Adriansyah et al.~\citep{Adriansyah:2011ta,vanderAalst:2012kf} compute alignments between an event trace and a labeled net using the $A^*$ algorithm and sophisticated heuristics.
Based on these alignments, they also introduce the precision measure~\citep{Adriansyah:2013fg} that we employ in \autoref{cha:discovery}.

All approaches in \citep{Cook:1999je,Cook:2001cr,Rozinat:2008iz,Goedertier:2009wl,Adriansyah:2011ta,vanderAalst:2012kf} focus on a closed system by relating observed process behavior (i.e., executed activities) to a process model.
In contrast, we consider the interaction between (multiple) open systems, and relate observed interaction behavior (i.e., sent or received messages, possibly from two different viewpoints) to an open system model.
In addition, replaying an event trace $w$ on a labeled $N$ without any mismatch but without reaching a final marking of $N$ is considered erroneous in~\citep{Rozinat:2008iz,Adriansyah:2011ta,vanderAalst:2012kf};
in other words, \citep{Rozinat:2008iz,Adriansyah:2011ta,vanderAalst:2012kf} implicitly assume $w$ to reach a final marking of $N$.
In contrast, we do not make any assumptions about $w$ and $N$.
Therefore, existing replay techniques require event logs of higher quality~\citep{vanderAalst:2012um} (e.g., event logs with complete event traces), whereas our approach also works with event logs of lower quality (e.g., event logs with incomplete event traces).

In our setting, Van der Aalst et al.~\citep{Aalst:2008jc} map a service contract specified in WS-BPEL~\citep{Jordan:2007uu} onto a workflow net~\citep{vanderAalst:1998tl} (which, in that case, can be seen as the inner net resembling the replay environment) and employ conformance checking techniques from process mining~\citep{Rozinat:2008iz} on this workflow net.
In contrast, we can measure the deviation of an implementation from its specification with respect to all possible $\bb${\hyph}conforming implementations; if there exists a deviation, then the implementation does not $\bb${\hyph}conform to the specification.
In addition, the approach in \citep{Aalst:2008jc} does not allow for a finite characterization of all implementations---in contrast to the maximal $\bb${\hyph}partner in \autoref{cha:bounded}.

\citet{Comuzzi:2012io} investigate online conformance checking (that is, conformance checking with incomplete event traces) using a weaker refinement notion than our notion of $\bb${\hyph}conformance.
Different conformance relations on a concurrency{\hyph}enabled model have been studied by \citet{PoncedeLeon:2012gg}.
As their considered conformance relations differ from $\bb${\hyph}conformance, their work is not applicable in our setting.
Also, maximal partners have not been studied yet in the setting of~\citep{PoncedeLeon:2012gg}.

\citet{MotahariNezhad:2010jz} investigate event correlation; that is, they try to find relationships between events that belong to the same process execution instance.
In contrast to event correlation, we do not vary the system instances, but consider a conformance relation of an unknown implementation to the known specification.

Our notion of conformance testing is also called monitoring~\citep{Rozinat:2008iz} or passive testing~\citep{Tretmans:1999eo}:
We solely rely on the given specification and the observed behavior recorded by the event log, and have no control over the test case (i.e., the open system that communicates with the unknown implementation and from whose communication the provided event log originates).
Our passive testing approach is opposed to active testing, where a tester has active control over the test environment and especially a set of predefined tests that are executed~\citep{Tretmans:1999eo,Brinksma:2001wp}.
For example, \citet{Kaschner:2011fi} constructs test cases from the operating guideline that we described in \autoref{sec:tout_related} to actively test for conformance of asynchronously communicating services.

\citet{Brinksma:2001wp} present an annotated bibliography of test theory that is based on labeled transition systems.
Another approach for formal testing is based on Mealy finite state machines~\citep{Mealy:1955ta} (also known as the FSM-approach); for overviews of the FSM-approach see~\citep{Lee:1996kg,Petrenko:2001hm}.
The link between the FSM-approach and test theory based on labeled transition systems is studied by \citet{Tan:1998wp}.

Note that our testing approach is not restricted to $\bb${\hyph}conformance;
in general, we can test for every conformance relation that allows to compute a finite maximal partner.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Work related to open system discovery}\label{sec:aout_system_discovery}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, we review work related to our discovery approach in \autoref{cha:discovery}.

Discovering a formal model from observed behavior recorded in event logs is studied in the area of process mining~\citep{vanderAalst:2011wg}, as already explained in \autoref{sec:aout_conformance_testing}.
There exists a variety of discovery algorithms; for example, the $\alpha$-algorithm~\citep{vanderAalst:2004cj}, the ILP-miner~\citep{Werf:2009tw}, the heuristics miner~\citep{Weijters:2003tn}, and genetic discovery algorithms~\citep{Medeiros:2007hw,Buijs:2012es}.
These discovery algorithms are all tailored toward closed systems.
They discover a formal process model from an event log that recorded process activities.
In contrast, the presented discovery algorithm in \autoref{cha:discovery} operates in the setting of open systems.
We discover a formal open system model from an event log that recorded communication behavior between two running open systems.

In the area of service-oriented computing~\citep{Papazoglou:2008uk}, the term ``discovery'' is ambiguous:
On the one hand, discovery describes techniques for producing a service model from observed communication behavior of services~\citep{vanderAalst:2012um},
and one the other hand, discovery describes techniques for finding a service model in a service repository in service-oriented architectures~\citep{Papazoglou:2008uk}.
Process mining research has been focused on processes but during the last few years, process mining techniques have also been applied to services resulting in the term ``service mining''.
Van der Aalst~\citep{vanderAalst:2012fv} reviews service mining research and identifies two main challenges regarding the discovery of services:
(1) the correlation of instances of a service with instances of another service (e.g.,~\citep{Basu:2008fm,MotahariNezhad:2010jz}), and
(2) the discovery of services based on observed behavior (e.g.,~\citep{Dustdar:2006ws,Asbagh:2007vf,Tang:2010dz,Musaraj:2010cx,MotahariNezhad:2008wq}).
A service can be seen as an open system, thus \autoref{cha:discovery} contributes to the second challenge.

\citet{Dustdar:2006ws} discover workflow models from service interaction.
The authors of~\citep{Asbagh:2007vf,Tang:2010dz} discover workflow models from interaction patterns.
However, these approaches can only discover parts of a (complex) service in the form of service composition pattern,
whereas our discovery algorithm produces a complete (service) model.

\citet{Musaraj:2010cx} correlate messages from an event log without correlation information and use this information in their discovery algorithm. In contrast, we abstract from correlation information and assume cases to be independent.
Another difference is that our discovered model $\bb${\hyph}conforms to a given open system model $\spec$ and it balances the four conflicting quality dimensions with respect to a given event log, guided by user preferences.

\citet{MotahariNezhad:2008wq} present a user-driven refinement approach for discovering service models.
In essence, their approach considers the fitness and the precision dimension, but ignores generalization and simplicity of the discovered service model.
Like \citet{Musaraj:2010cx}, \citet{MotahariNezhad:2008wq} do not assume a service model to be given and, thus, they cannot guarantee that their produced service model can interact correctly (i.e., $\bb$-responsively) with its environment.

The idea of using a genetic algorithm for discovery is inspired by the work of \citet{Medeiros:2007hw}.
\citet{Buijs:2012es,Buijs:2012jr} use a genetic algorithm to discover sound workflow models while balancing the four conflicting quality dimensions.
In \autoref{sec:discovery_procedure}, we discussed the relation of our measures for these four quality dimensions and the measures used in~\citep{Buijs:2012es,Buijs:2012jr}.
For the simplicity measure, we used the structure of the LTS $\CSD(\max)$, which does not exist for workflow models.
Correctness in our setting is $\bb${\hyph}responsiveness, which is a weaker criterion than soundness in~\citep{Buijs:2012jr};
soundness additionally requires proper termination.
To deal with $\bb${\hyph}responsiveness in the setting of open systems, we assume an open system $\spec$ to be given and we discover,
from observed behavior of $\spec$ and its environment, an open system $\imp$ that is guaranteed to $\bb${\hyph}conform to $\spec$.
