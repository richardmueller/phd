#!/bin/bash

# We export as pdf per default
if [ $# -eq 1 ]; then
    FORMAT="pdf"
    INPUT_FILE=$1
    OUTPUT_FILE=`echo $1 | sed -e 's/.graffle//'`
elif [ $# -eq 2 ]; then
    FORMAT=""
    INPUT_FILE=$1
    OUTPUT_FILE=$2
elif [ $# -eq 3 ]; then
    FORMAT=$1
    INPUT_FILE=$2
    OUTPUT_FILE=$3
else
    PROG=`basename $0`
    echo "Usage: $PROG [<format>] <graffle file> <outputfile>"
    exit 1
fi

# Ensure absolute pathname for INPUT
if echo "$INPUT_FILE" | grep '^/'; then
    INPUT_PATH=$INPUT_FILE
else
    INPUT_PATH=$PWD/$INPUT_FILE
fi

# Ensure absolute pathname for OUTPUT
if echo "$OUTPUT_FILE" | grep '^/'; then
    OUTPUT_PATH=$OUTPUT_FILE
else
    OUTPUT_PATH=$PWD/$OUTPUT_FILE
fi

DIR=`dirname $0`

#echo Format = $FORMAT
echo Input = $INPUT_PATH
#echo Output = $OUTPUT_PATH

osascript $DIR/graffle.scpt "$FORMAT" "$INPUT_PATH" "$OUTPUT_PATH"
