%!TEX root = ../rmueller_phd.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter*{\myTitle}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\selectlanguage{american}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Abstract}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Best engineering practices suggest \emph{specifying} a system before actually \emph{implementing} it.
Both the implementation as well as its specification exhibit \emph{behavioral properties}.
\emph{Conformance checking} is deciding whether the implementation of a system preserves a certain behavioral property of its specification.
This is the central scientific problem of this thesis.

Over the past years, there has been a shift in systems engineering from monolithic, closed systems to distributed systems, composed of \emph{open systems}.
Therefore, our research centers around conformance checking for open systems.
An open system interacts with other open systems---that is, its \emph{environment}.
Of particular interest are \emph{responsive} environments with which interaction or mutual termination is always possible.
We refer to such an environment as a \emph{partner}.
For an open system, conformance checking translates to deciding whether each partner of its specification is a partner of the implementation.

We consider conformance checking for open systems in two distinct scenarios.
In the first scenario, the \emph{model-model scenario}, we assume the specification and the implementation of an open system to be given as formal models.
We characterize conformance for two variants of responsiveness.
For the first variant, conformance turns out to be undecidable.
For the second variant however, we develop a decision algorithm for conformance and a finite characterization of all conforming open systems.
In addition, two open systems can be composed, yielding again an (open) system.
In general, we require conformance to respect \emph{compositionality}; that is, we wish to infer the conformance of a composition from the conformance of the composed open systems.
Therefore, we also study the above mentioned compositionality property of conformance for the two variants of responsiveness, and show its (un-)decidability.

In the second scenario, the \emph{log-model scenario}, we assume the specification of an open system to be given as a formal model, but this time no formal model of the implementation is available.
However, most implementations record their actual behavior.
The observed behavior of an implementation can be recorded in an \emph{event log}.
This is a more realistic and practically relevant assumption because the implementation is often too complex to be formally modeled.
The idea is to use an event log to check conformance of the unknown implementation to its known specification.
To this end, we present a necessary condition for conformance:
We analyze whether there exists a conforming implementation which can produce the event log.
Furthermore, we study whether we can discover a formal model of the unknown implementation from the event log, assuming the implementation conforms to its specification.

We implement the decision algorithm from the first scenario and use it to develop algorithms for both questions in the second scenario.
We evaluate the implemented algorithms using industrial-sized specifications and event logs.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \chapter*{\myTitle}
\newpage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\selectlanguage{ngerman}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{Kurzfassung}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Es ist gute ingenieurwissenschaftliche Praxis, ein System vor seiner Implementierung zu spezifizieren.
Sowohl die \emph{Implementierung} als auch die \emph{Spezifikation} eines Systems zeigen \emph{Verhaltenseigenschaften}.
Eine Implementierung eines Systems ist \emph{konform} zu einer Spezifikation, wenn die Implementierung bestimmte Verhaltenseigenschaften der Spezifikation bewahrt.
Diese Konformanz zu entscheiden ist die zentrale wissenschaftliche Fragestellung dieser Arbeit.

In der Systementwicklung hat es in den letzten Jahren eine Verschiebung weg von monolithischen, geschlossenen Systemen hin zu verteilten, \emph{offenen Systemen} gegeben.
Daher beschäftigen wir uns in dieser Arbeit mit der Konformanz offener Systeme.
Ein offenes System interagiert mit anderen offenen Systemen, d.h. mit seiner \emph{Umgebung}.
Von besonderem Interesse sind hierbei \emph{responsive} Umgebungen, mit welchen Interaktion oder gemeinsame Terminierung immer möglich ist.
Solch eine responsive Umgebung ist ein \emph{Partner} eines offenen Systems.
Die Frage nach der Konformanz eines offenen Systems lässt sich damit in die Frage übersetzen, ob jeder Partner der Spezifikation auch ein Partner der Implementierung ist.

In dieser Arbeit betrachten wir die Konformanz offener Systeme in zwei unterschiedlichen Szenarien.
Im ersten Szenario, dem \emph{Modell-Modell-Szenario}, ist sowohl die Spezifikation als auch die Implementierung eines offenen Systems als formales Modell gegeben.
Wir charakterisieren Konformanz für zwei Varianten von Responsivität und zeigen deren (Un-)Entscheidbarkeit.
Im Fall der Entscheidbarkeit entwickeln wir einen Entscheidungsalgorithmus und eine endliche Charakterisierung aller konformen offenen Systeme.
Die Komposition zweier offener Systeme ist wieder ein offenes System.
Eine wünschenswerte Eigenschaft von Konformanz ist daher \emph{Kompositionalität}, d.h. wir wollen von der Konformanz der komponierten offenen Systeme auf die Konformanz der Komposition schließen.
Dementsprechend untersuchen wir Konformanz für beide Varianten von Responsivität auch auf Kompositionalität und zeigen deren (Un-)Entscheidbarkeit.

Im zweiten Szenario, dem \emph{Log-Modell Szenario}, ist nur die Spezifikation eines offenen Systems als formales Modell gegeben; ein formales Modell der Implementierung ist nicht bekannt.
Jedoch stellen die meisten Implementierungen beobachtetes Verhalten von sich in Form eines \emph{Logs} zur Verfügung.
Diese Annahme ist weitaus realistischer und praktisch relevanter als das Modell-Modell-Szenario, da die Implementierung eines offenen Systems in der Regel zu komplex ist um formal modelliert zu werden.
Die Idee ist nun, mit Hilfe des Logs auf die Konformanz der unbekannten Implementierung und der bekannten Spezifikation zu schließen.
Zu diesem Zweck präsentieren wir in dieser Arbeit eine notwendige Bedingung für Konformanz:
Wir entscheiden, ob eine konforme Implementierung existiert, welche das gegebene Log erzeugen kann.
Darüber hinaus entwickeln wir eine Methode zum automatischen Erstellen eines formalen Modells der unbekannten Implementierung aus dem gegebenen Log unter der Annahme, dass die Implementierung konform zur Spezifikation ist.

Wir implementieren den Entscheidungsalgorithmus aus dem ersten Szenario und verwenden ihn, um Algorithmen für beide Fragen im zweiten Szenario zu entwickeln.
Wir werten die implementierten Algorithmen mit industrienahen Spezifikationen und Logs aus.

\selectlanguage{american}